﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DoorTrigger : MonoBehaviour
{
    [SerializeField] GameObject door;
    public bool isDoorOpened = false;

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            door.transform.position += new Vector3(0, 8, 0);
            isDoorOpened = true;
            
        }


     }

    private void OnTriggerExit(Collider other)
    {
        if (other.tag == "Player")
        {
            door.transform.position -= new Vector3(0, 4, 0);
            isDoorOpened = false;
        }


    }
}