﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TimeControl : MonoBehaviour
{

    public float quantoVeloce = 1f;
    
    // Start is called before the first frame update
    void Start()
    {
        Time.timeScale = quantoVeloce;
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
