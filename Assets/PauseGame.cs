﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PauseGame : MonoBehaviour
{
    bool isPaused = false;
    [SerializeField] GameObject PausaUI;
    private void Update()
    {

        if (Input.GetKeyDown(KeyCode.P))
        {
            Debug.Log("ciao");
            isPaused = true;
        }

        if (isPaused == true)
        {
            PausaUI.SetActive(true);
            Time.timeScale = 0;       

        }

    }
    public void ResumeGameFromPause()
    {
        Time.timeScale = 1;
    }




}
