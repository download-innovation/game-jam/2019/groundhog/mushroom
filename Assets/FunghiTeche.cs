﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FunghiTeche : MonoBehaviour
{
    Progression progression;
    public bool statoRidotto = false;
    bool timerSet = false;
    public float timer = 300f;
    [Range (0, 100)]public float costituzione = 3f;
    public float costituzioneIncrese = 1;
    bool isIncreasing;

    private void Awake()
    {
        progression = FindObjectOfType<Progression>();
    }
    // Start is called before the first frame update
    void Start()
    {
        
        isIncreasing = true;
        this.gameObject.GetComponent<Transform>().localScale -= new Vector3(18f, 18f, 18f);
        progression.AddFunghiCount();
    }

    // Update is called once per frame
    void Update()
    {
       
        
        if (isIncreasing == true)
        { 
            costituzione += costituzioneIncrese * Time.deltaTime;
            this.gameObject.GetComponent<Transform>().localScale += new Vector3(costituzione / (costituzione * 150f), costituzione / (costituzione * 150f), costituzione / (costituzione * 150f));
        }
        if (costituzione >= 100)
        {
            isIncreasing = false;
            
        }
        

        if (costituzione <= 2 && statoRidotto == false)
        {
            timerSet = true;
            statoRidotto = true;
       
        }

        if (timerSet == true)
        {
             timer -= Time.deltaTime;
        }

        if (timer <= 0)
        {
             costituzioneIncrese = 1f;
             isIncreasing = true;
             timer = 10;
        }

        if(statoRidotto == true)
        {
            StatoRidotto();
           
        }
    }

 

    public void StatoRidotto()
    {
        progression.AddFunghiRidottiCount();
    }

    public void RiduciCostituzione()
    {
        costituzione -= 2 * Time.deltaTime; 

    }
}
  
