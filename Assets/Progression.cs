﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Progression : MonoBehaviour
{
    public int funghiRidottiCount = 0;
    public int funghiCount = 0;
    
    [SerializeField] GameObject redSirenRoom1;
    [SerializeField] GameObject greenSirenRoom1;
    [SerializeField] GameObject redLightRoom1;
    [SerializeField] GameObject greenLightRoom1;
    [SerializeField] GameObject redLightRoom2;
    [SerializeField] GameObject greenLightRoom2;
    [SerializeField] GameObject redLightRoom3;
    [SerializeField] GameObject greenLightRoom3;


    [SerializeField]GameObject door1;
    GameObject door2;
    GameObject door3;
    FunghiTeche funghiTeche;
    public bool stanza1Completata = false;
    public bool stanza2Completata = false;
    public bool stanza3Completata = false;

    public bool stanza1Fungo1 = false;
    public bool stanza1Fungo2 = false;
    public bool stanza1Fungo3 = false;
    public bool stanza1Fungo4 = false;
    public bool stanza1Fungo5 = false;
    public bool stanza1Fungo6 = false;

    public bool stanza2Fungo1 = false;
    public bool stanza2Fungo2 = false;
    public bool stanza2Fungo3 = false;
    public bool stanza2Fungo4 = false;
    public bool stanza2Fungo5 = false;
    public bool stanza2Fungo6 = false;

    public bool stanza3Fungo1 = false;
    public bool stanza3Fungo2 = false;
    public bool stanza3Fungo3 = false;
    public bool stanza3Fungo4 = false;
    public bool stanza3Fungo5 = false;
    public bool stanza3Fungo6 = false;


    private void Awake()
    {
        funghiTeche = FindObjectOfType<FunghiTeche>();
    }

    void Start()
    {
           
    }

    // Update is called once per frame
    void Update()
    {
        if(funghiCount == funghiRidottiCount)
        {
            stanza1Completata = true;
        }
        
        if (stanza1Completata == true)
        {
            Room1Completed();
        }
    }

    public void SetToTrue()
    {
       
    }

    void Room1Completed()
    {
        redSirenRoom1.SetActive(false);
        greenSirenRoom1.SetActive(true);
        redLightRoom1.SetActive(false);
        greenLightRoom1.SetActive(true);
        door1.SetActive(true);
    }
    
    public void AddFunghiCount()
    {
        funghiCount++;
    }

    public void AddFunghiRidottiCount()
    {
        funghiRidottiCount++;
    }



}
