﻿using UnityEngine;

public class ChangeColor : MonoBehaviour
{
    // Interpolate light color between two colors back and forth

    public bool roomCompleted = false;
    Light lt;

    void Start()
    {
  
        lt = GetComponent<Light>();
        lt.color = Color.red;
    }

    void Update()
    {
        if(roomCompleted == true)
        {
            lt.color = Color.green;
        }
     
    }

    public void ChangeLightColor()
    {
        roomCompleted = true;
    }
}
