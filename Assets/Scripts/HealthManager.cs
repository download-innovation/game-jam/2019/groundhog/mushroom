﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;


public class HealthManager : MonoBehaviour {

   // int loadedBullets;
   // WeaponBase weaponBase;
    public Slider sliderSalute;
    //public Slider sliderEnergia;
    Animator animator;
    FunghiTeche funghiTeche;
	[Range(0,100)][SerializeField] private float health = 100f;
    public float healthReducer = 1f;
    public float recharge;
    //public int rechargeWeapon;
    public bool isPlayer = false;
	public bool removeColliderOnDeath = false;
	public HealthManager referer;	// Special prorperty for create multiple hit system, if it sets on GameObject that has same HealthManager, Apply Damage to it.
	public int damageFactor = 1;
	public Text healthText;
    public float recoveryHealth = 1f;


    void Start() {

       // weaponBase = FindObjectOfType<WeaponBase>();
        //loadedBullets = weaponBase.loadedBullets;
     
       
       if(isPlayer == true)
        {
            sliderSalute.minValue = 0;
            sliderSalute.maxValue = 100;
            sliderSalute.wholeNumbers = true;
        }
        
        //sliderEnergia.minValue = 0;
        //sliderEnergia.maxValue = 199;
        //sliderEnergia.wholeNumbers = true;


        funghiTeche = FindObjectOfType<FunghiTeche>();
		animator = GetComponent<Animator>();
        //rechargeWeapon = FindObjectOfType<WeaponBase>().loadedBullets;
        if (isPlayer) healthText = GameObject.Find("UI/InGameUI/CharacterStatus/HealthText").GetComponent<Text>();
	}

	void Update() {
        //loadedBullets = weaponBase.loadedBullets;

        
        //sliderEnergia.value = loadedBullets;

        if (healthText) {
			healthText.text = "HP: " + health.ToString();
		}

        if(health >= 100f)
        {
            health = 100f;
        }

        if(isPlayer == true)
        {
           health -= healthReducer * Time.deltaTime;
            sliderSalute.value = health;
        }

        
    }
	
	public void ApplyDamage(float damage) {
		if(IsDead) return;

		damage *= damageFactor;

		if(referer) {
			referer.ApplyDamage(damage);
		}
		else {
			health -= damage;
            /*if (tag == "Enemy")
            {
                this.gameObject.GetComponent<Transform>().localScale -= new Vector3(0.2f, 0.2f, 0.2f);
            }*/
      
			if(health <= 0) {
				health = 0;
				
				if(animator) {
					animator.SetTrigger("Dead");
				}
				
				if(removeColliderOnDeath) {
					RemoveColliders(GetComponents<Collider>());
					RemoveColliders(GetComponentsInChildren<Collider>());
				}
			}
		}
	}


    public void ReduceSize(float damage)
    {
        if (IsDead) return;

        damage *= damageFactor;
        
       

        if (referer)
        {
            referer.ApplyDamage(damage);
            
        }
        else
        {
          
           // health -= damage;
            if (tag == "Enemy")
            {

                funghiTeche.RiduciCostituzione();
                this.gameObject.GetComponent<Transform>().localScale -= new Vector3(0.2f, 0.2f, 0.2f);
                if (this.gameObject.GetComponent<Transform>().localScale.x <= 0.2f && this.gameObject.GetComponent<Transform>().localScale.y <= 0.2f && this.gameObject.GetComponent<Transform>().localScale.z <= 0.2f)
                {
                    this.gameObject.GetComponent<Transform>().localScale = new Vector3(0.4f, 0.4f, 0.4f);
                }
            }

            if (health <= 0)
            {
                health = 0;

                if (animator)
                {
                    animator.SetTrigger("Dead");
                }

                if (removeColliderOnDeath)
                {
                    RemoveColliders(GetComponents<Collider>());
                    RemoveColliders(GetComponentsInChildren<Collider>());
                }
            }
        }
    }

    public void SetHealth(float newHealth) {
		health = newHealth;
	}

	public bool IsDead {
		get {
			if(!referer) {
				return health <= 0;
			}
			else {
				return referer.IsDead;
			}
		}
	}

	void RemoveColliders(Collider[] colliders) {
		foreach(Collider collider in colliders) {
			collider.enabled = false;
		}
	}
    public void RecoveryHealth()
    {
        health = health + recoveryHealth;
    }
}
