﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class StartupText : MonoBehaviour {

    ScoreManager scoreManager;
    public bool startScoreCount = false;
    Text text;
	GlobalSoundManager globalSoundManager;
	public int remainSeconds;
	
	public AudioClip prepare;
	public AudioClip gameBegins;
	public AudioClip beep;

	void Start() {
        scoreManager = FindObjectOfType<ScoreManager>();
		globalSoundManager = GlobalSoundManager.Get();
		globalSoundManager.Play(prepare);

		text = GetComponent<Text>();
		remainSeconds = 2;

		StartCoroutine(StartAnimation());
	}

	IEnumerator StartAnimation() {
		for(int i = remainSeconds; i > 0; i--) {
			if(i <= 5)
            {
                globalSoundManager.Play(beep);
               
            }
            if (i <= 1)
            {
               
                scoreManager.StartScore();
            }

            UpdateText(i);
			yield return new WaitForSeconds(1f);
		}

		text.text = "FIGHT!";
		globalSoundManager.Play(gameBegins);

		yield return new WaitForSeconds(3f);
		Destroy(gameObject);
	}

    public void StartScoreCount()
    {
        startScoreCount = true;
    }

    void UpdateText(int sec) {
		text.text = "Prepare to fight...\nBegins at " + sec + " seconds.";
	}
}
