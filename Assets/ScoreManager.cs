﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;

public class ScoreManager : MonoBehaviour
{
    bool startScore = false;
    StartupText startupText;
    public float scoreAmount;
    public int pointPerSecond;
    public int playerPoints;
    //public TextMeshProUGUI endLevelPointsText;
    public TextMeshProUGUI PlayerPointsText;
    //private int endLevelPoints;
    public int maximumPoints;

    // Start is called before the first frame update
    void Start()
    {
       
        scoreAmount = 0f;
        pointPerSecond = 1;
   
    }

    // Update is called once per frame
    void Update()
    {
        if (startScore == true)
        {
            float progress = (float)playerPoints / (float)maximumPoints;
            float progressSlider = Mathf.Clamp01(progress);
            scoreAmount += pointPerSecond * Time.deltaTime;
            playerPoints = (int)scoreAmount;
            //endLevelPoints = playerPoints;
            PlayerPointsText.text = "Score: " + playerPoints.ToString();
            //endLevelPointsText.text = endLevelPoints.ToString();
        }
    }

    public void StartScore()
    {
        startScore = true;
    }
    }

