# Fasi della Game Jam
## Fase 1 - Formazione team
Entro le ore 14:00
* Caricare il **logo** (immagine, qualsiasi formato, 1024x1024 pixel)  
  ![Logo](./examples/logo.png)
* Compilare il file `jam.json`, specificando il **path** del logo all'interno del repository:

```json
{
  "teamLogo": "/path/to/logo.png"
}
```

## Fase 2 - Ideazione gioco
Entro la mezzanotte
* Caricare il **banner** con il titolo del gioco (immagine, qualsiasi formato, 2000x250 pixel)  
  ![Banner](./examples/banner.png)
* Caricare la **grafica promozionale** (immagine, qualsiasi formato, 4096x2160 pixel)  
  ![Promo](./examples/promo.png)  
  Attenzione alle aree che potrebbero essere occupate da altri elementi grafici durante le presentazioni  
  ![Promo sovrapposizioni](./examples/promo-mask.png)
* Compilare il file `jam.json`, specificando i **path** degli elaborati all'interno del repository:

```json
{
  "teamLogo": "/path/to/logo.png",
  "banner":"/path/to/banner.jpg",
  "promo": "/path/to/promo.bmp"
}
```

## Fase 3 - Realizzazione
Entro le 16:30 circa
* Caricare il codice e gli asset sul repository
* Possibilmente lasciare indicazioni sulla compilazione, l'installazione e l'uso
    - In questo file, modificandolo con l'editor incorporato
    - Nella [Wiki](./wiki) (bottone in alto, sotto il nome del repository)

# Git
[Git](https://git-scm.com/) è un sistema di controllo di versione, che permette di mantenere più versioni di uno stesso progetto, realizzate in tempi diversi e/o da persone differenti.

I file del progetto sono salvati in un _repository remoto_ centrale.

Chi intende apportare una modifica ai file deve prima scaricarli sulla macchina locale, creando un _repository locale_, contenuto in una cartella `.git`. Il repository locale _traccia_ quello remoto: essi condividono la storia di tutte le modifiche passate e future.

Associata al repository locale c'è una _working directory_. È la cartella dove possiamo vedere lo stato corrente del progetto e apportare le nostre modifiche.

Man mano che apportiamo modifiche, possiamo aggiungerle all'_area di staging_, dove sono pronte per essere incorporate nella cronologia del repository **locale**.

In ogni momento possiamo effettuare un _commit_ delle modifiche presenti in staging. Ogni commit rappresenta un punto ripristinabile nella cronologia delle modifiche.

Per condividere le nostre modifiche con il resto del gruppo occorre inviare i commit locali al repository centrale.

> Se non siete esperti di Git, non è questo il momento di imparare a usarlo. I pochi comandi che seguono però vi permetteranno di inviare il vostro codice al termine della Jam.

Gitea ci permette di apportare modifiche ai files online, tramite l'editor incorporato. Ciò può essere molto comodo per piccole modifiche, anche se non è fattibile per sviluppare un intero progetto.

## Comandi essenziali
Sostituire `<parametro>` con il valore che si applica nel vostro caso.

#### `git clone <repository remoto>` oppure `git clone <repository remoto> <working directory>`
Copia tutti i file versionati dal `<repository remoto>` in una cartella locale, che sarà la nuova working directory. 
È possibile specificare un nome per la cartella passando il parametro `<working directory>`

Copy all versioned files from remote repository to local machine. Create a new working directory. Optionally specify working directory name
git add <folder>
git add <file>
Stage all changes in <folder> for the next commit. Alternatively stage only specified <file>
git commit -m "<message>"
git commit
Commit all staged modifications (see git add). Comment the action with <message> to the action. If the –m option is omitted, a text editor is launched (useful to add more complex comments)
git status
List staged, not staged and untracked files
git diff
List unstaged changes, showing the files and the tracked difference
git log
git log -n <number>
Show the commit history. If -n option is used, only <number> latest commits are shown
git push
Push the current branch to the remote repository with all its commits
git pull
Fetch the remote repository and merge it into the local working directory